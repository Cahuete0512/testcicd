from src import utils

def test_is_prime():
    assert utils.is_prime(4) == False
    assert utils.is_prime(2) == True
    assert utils.is_prime(3) == True
    assert utils.is_prime(8) == False
    assert utils.is_prime(10) == False
    assert utils.is_prime(7) == True
    assert utils.is_prime(-3), "Negative numbers are not allowed"

def test_cubic():
    assert utils.cubic(2) == 8
    assert utils.cubic(-2) == -8
    assert utils.cubic(2) != 4
    assert utils.cubic(-3) != 27

def test_hello():
    assert utils.say_hello("Eline") == "Hello, Eline"
    assert utils.say_hello("Adeline") == "Hello, Adeline"
    assert utils.say_hello("Romain") != "Hello, Maxine"
    assert utils.say_hello("Pierre") != "Hello, FELIC LE BG IL EST TROP BEAU"

